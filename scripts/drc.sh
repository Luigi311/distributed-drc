#!/usr/bin/env bash

# Source: http://mywiki.wooledge.org/BashFAQ/035
die() {
    printf '%s\n' "$1" >&2
    printf '%s\n' "Removing ${FILE}"
    rm "${FILE}"
    exit 1
}

CONTAINER="mka"
CODEC="aac"
BITRATE="192k"
TITLE="Stereo DRC"
COMPRESSION="4"

# Source: http://mywiki.wooledge.org/BashFAQ/035
while :; do
    case "$1" in
        -h | -\? | --help)
            help
            exit 0
            ;;
        -i | --input)
            if [ "$2" ]; then
                INPUT="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        --container)
            if [ "$2" ]; then
                CONTAINER="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -c | --codec)
            if [ "$2" ]; then
                CODEC="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        --compression)
            if [ "$2" ]; then
                COMPRESSION="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -b | --bitrate)
            if [ "$2" ]; then
                BITRATE="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -t | --title)
            if [ "$2" ]; then
                TITLE="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        --) # End of all options.
            shift
            break
            ;;
        -?*)
            die "Error: Unknown option : $1"
            ;;
        *) # Default case: No more options, so break out of the loop.
            break ;;
    esac
    shift
done

STREAMS=($(ffprobe -hide_banner -loglevel error -i "${INPUT}" -show_streams -select_streams a | grep index | awk -F'=' '{print $2}'))

for index in "${STREAMS[@]}"
do
    CHANNELS=$(ffprobe -hide_banner -loglevel error -i "${INPUT}" -show_streams -select_streams ${index} | grep channels | awk -F'=' '{ print $2 }')

    if [ $CHANNELS -gt 2 ]; then
        LANGUAGE=$(ffprobe -hide_banner -loglevel error -i "${INPUT}" -show_streams -select_streams ${index} | grep language | awk -F'=' '{ print $2 }')
        FILE="${INPUT%.*}.${LANGUAGE}.Stereo-DRC.${CONTAINER}"
        LOG=$(ffmpeg -y -hide_banner -loglevel error -i "${INPUT}" -map 0:"${index}" -c:a:0 "${CODEC}" -ac 2 -filter:a:0 "acompressor=ratio=${COMPRESSION},loudnorm" -b:a:0 "${BITRATE}" -metadata:s:a:0 title="${TITLE}" -metadata:s:a:0 language="${LANGUAGE}" "${FILE}" 2>&1)

        if [ -n "$LOG" ]; then
            die "$LOG"
        fi
    fi

done
