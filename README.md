# distributed-drc
Run ffmpeg drc compression and loudnorm on audio tracks that are greater than 2 channels and create a file right next to the original file. Useful for using plex with external audio file support located at [luigi311/plexmediaserver-customaudio](https://hub.docker.com/repository/docker/luigi311/plexmediaserver-customaudio)
