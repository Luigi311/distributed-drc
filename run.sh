#!/usr/bin/env bash

# Source: http://mywiki.wooledge.org/BashFAQ/035
die() {
    printf '%s\n' "$1" >&2
    exit 1
}

help() {
    help="$(cat <<EOF
ffmpeg drc compression and loudnorm on audio tracks that are greater than 2 channels and create a file right next to the original file
Usage:
    ./run.sh [options]
Example:
    ./run.sh -i /mnt/Media/Plex
General Options:
    -h/--help                       Print this help screen
    -i/--input          [folder]    Folder containing all video files to encode                              (default video.mkv)
    -j/--jobs           [string]    Amount of parallel encodes to run                                        (default 10%)
    -l/--log            [string]    Log file to use to allow multiple instances to run/resuming              (default encoding.log)
    -e/--extension      [string]    Extension to find video files to encode                                  (default mkv)
    --distribute                    Distribute encoding to multiple machines from ~/.parallel/sshloginfile   (default false)
    --resume                        Resume option for parallel, will use encoding.log and vmaf.log           (default false)
Encoding Settings:
    -c/--codec          [string]    Audio codec to use                                                       (default aac)
    -b/--bitrate        [string]    Bitrate to encode audio to                                               (default 192k)
    -t/--title          [string]    Metadata title for encoded audio                                         (default Stereo DRC)
    --container         [string]    Container to export encoded audio to                                     (default mka)
EOF
)"
    echo "$help"
}

INPUT="-1"
EXTENSION="mkv"
CONTAINER="mka"
CODEC="aac"
BITRATE="192k"
TITLE="Stereo DRC"
COMPRESSION="4"
JOBS="10%"
LOG="encoding.log"

# Source: http://mywiki.wooledge.org/BashFAQ/035
while :; do
    case "$1" in
        -h | -\? | --help)
            help
            exit 0
            ;;
        -i | --input)
            if [ "$2" ]; then
                INPUT="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -e | --extension)
            if [ "$2" ]; then
                EXTENSION="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -c | --container)
            if [ "$2" ]; then
                CONTAINER="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        --codec)
            if [ "$2" ]; then
                CODEC="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -b | --bitrate)
            if [ "$2" ]; then
                BITRATE="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -t | --title)
            if [ "$2" ]; then
                TITLE="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        --compression)
            if [ "$2" ]; then
                BITRATE="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        --resume)
            RESUME="--resume --resume-failed"
            ;;
        --distribute)
            DISTRIBUTE="--sshloginfile .. --workdir . --sshdelay 0.2"
            ;;
        -j | --jobs)
            if [ "$2" ]; then
                JOBS="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -l | --log)
            if [ "$2" ]; then
                LOG="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        --) # End of all options.
            shift
            break
            ;;
        -?*)
            die "Error: Unknown option : $1"
            ;;
        *) # Default case: No more options, so break out of the loop.
            break ;;
    esac
    shift
done

if [ "${INPUT}" == "-1" ]; then
    die "Input folder required"
fi

echo "Encoding"
find "${INPUT}" -name "*.${EXTENSION}" | parallel -j "${JOBS}" $DISTRIBUTE --joblog "${LOG}" $RESUME --bar "scripts/drc.sh" --input {} --container "${CONTAINER}" --codec "${CODEC}" --bitrate "${BITRATE}" --compression "${COMPRESSION}" --title "${TITLE}"
